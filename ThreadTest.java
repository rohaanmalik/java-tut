public class ThreadTest {
    public static void main(String[] args) {
        Runnable run = new MyRunnable();
        Thread thread1 = new Thread(run);
        thread1.start();

        Thread thread2 = new Thread(run);
        thread2.start();

        Runnable task1 = () -> {System.out.println(Thread.currentThread().getName() + "is running");};
        new Thread(task1).start();
    }
}