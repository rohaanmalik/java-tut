public abstract class Account implements Detailable {
    private double balance;
    private String name;
    private Currency currency;

    public Account(String name, double balance) throws DodgyNameException{
        setBalance(balance);
        setName(name);
    }

    public Account(String name, double balance, Currency currency) throws DodgyNameException{
        this(name, balance);
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }
    public void setBalance(double balance) {
        this.balance = balance;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) throws DodgyNameException {
        if (name == "Fingers") {
            throw new DodgyNameException();
        } else {
            this.name = name;
        }

    }

    public abstract String getDetails();

    public abstract void addInterest();

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }


}