import java.util.Comparator;

public class AccountComparator implements Comparator<Account> {

    @Override
    public int compare(Account o1, Account o2) {
        if (((Account)o1).getBalance() < ((Account)o2).getBalance()) {
            return -1;
        }

        else if (((Account)o1).getBalance() > ((Account)o2).getBalance()){
            return 1;
        }

        return 0;
    }


}