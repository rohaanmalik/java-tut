/**
 * Detailable
 */
public interface Detailable {

    public String getDetails();
}